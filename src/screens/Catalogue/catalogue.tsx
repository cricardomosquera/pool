import {Image , Row,  Col,} from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import {Data} from '../../utils/data'
import botellaAzul300 from '../../assets/Catalogo/AGUA-300-FONDO.webp'
import botellaVerde300 from '../../assets/Catalogo/AGUA-CON-GAS-300-FONDO.webp'
import botellaAzul1000 from '../../assets/Catalogo/AGUA-1000-FONDO.webp'
import botellaVerde600 from '../../assets/Catalogo/AGUA-CON-GAS-600-FONDO.webp'
import botellaAzul600 from '../../assets/Catalogo/AGUA-600-FONDO.webp' 
import { ImageProduct } from '../../shared/ImageProduct/imageProduct'

import energy300 from '../../assets/Catalogo/ENERGY-300-FONDO.webp'
import hidratante from '../../assets/Catalogo/HYDRATADE-500-FONDO.webp'
import soda400 from '../../assets/Catalogo/SODA-1700-1.webp'

import aguazaborizada from '../../assets/Catalogo/HPOOL-MANZANA-600-FONDO.webp'
import aguaLimon from '../../assets/Catalogo/858392389770_nizgndppyjbw_285843203869_plauriffpopr_1839581_1.jpeg'
import aguaLimonadaCoco from '../../assets/Catalogo/HPOOL-LIMONADA-600-FONDO.webp'
import limonadaMaracuya from '../../assets/Catalogo/HPOOL-MARACUYA-600-FONDO.webp'

import refrescoFruit from '../../assets/Catalogo/PASSION-FRUIT-400-FONDO.webp'
import salpicon from '../../assets/Catalogo/SALPICON-400-FONDO.webp'
import pinaNaranja from '../../assets/Catalogo/PINA-NARANJA-400-FONDO.webp'

import poolCola from '../../assets/Catalogo/catalogo-pool-02.jpg'
import poolNaranja from '../../assets/Catalogo/catalogo-pool-08-1536x1188.jpg'
import poolLimonada from '../../assets/Catalogo/catalogo-pool-03.jpg'

import poolColombia from '../../assets/Catalogo/catalogo-pool-05.jpg'
import poolUva from '../../assets/Catalogo/catalogo-pool-07-1536x1187.jpg'
import poolManzana from '../../assets/Catalogo/catalogo-pool-04.jpg'

export const Catalogue=()=>{
    const text = Data.catagolo

    return(
        <Container>
                <Row>
                    <div style={{display:'flex',height:'100%',width:'100%', backgroundColor:'#F1EFF0'}}  >
                        <div style={{width:'20%',height:'10%'}}>
                        <div style={{
                                    width:'0px',
                                    height:'0px',
                                    borderTop:'50px solid transparent',
                                    borderLeft:'100px solid #327EBD',
                                    marginTop:'30px',
                                    borderBottom:'50px solid transparent',
                        }}>
                            </div> 
                        </div>
                        <div style={{width:'80%',height:'10%',fontSize:30,marginTop:'30px'}}>
                            <Row>
                                {text.title}
                            </Row>
                            <Row style={{width:'80%',fontSize:20,marginLeft:'10%'}}>
                                {text.description}
                            </Row>

                        </div>
                    </div>
                </Row>
                <Row style={{background:'#F1EFF0',display:'flex',justifyContent:'space-between'}}>
                    <Col style={{margin:'auto'}}>
                        <ImageProduct title='300 ml'  src={botellaAzul300} />
                        <ImageProduct title='300 ml'  src={botellaVerde300} />
                    </Col>
                    <Col style={{margin:'auto'}}>
                        <ImageProduct title='1000 ml'  src={botellaAzul1000} />
                        
                    </Col>
                    <Col style={{margin:'auto'}}>
                        <ImageProduct title='600 ml'  src={botellaVerde600} />
                        <ImageProduct title='600 ml'  src={botellaAzul600} />
                    </Col>
                </Row>
                <Row style={{background:'#F1EFF0',display:'flex',justifyContent:'space-between',height:'800px',marginTop:'10px'}}>
                    <Col style={{margin:'0px 0px 0px 100px'}}>
                        <ImageProduct title='300 ml' description='Energizante' src={energy300} />
                    </Col>
                    <Col style={{margin:'auto'}}>
                        <ImageProduct title='500 ml' description='Hidratante'  src={hidratante} />
                        
                    </Col>
                    <Col style={{margin:'400px 50px 0px 0px'}}>
                            <ImageProduct title='400 ml - 1700 ml' description='Soda'  src={soda400} />
                        
                    </Col>
                </Row>
                <Row style={{background:'#F1EFF0',display:'flex',justifyContent:'space-between',height:'800px',marginTop:'10px'}}>
                    <Col style={{margin:'0px 0px 0px 100px'}}>
                        <ImageProduct title='300 ml -600 ml' description='Agua saborizada Manzana' src={aguazaborizada} />
                    </Col>
                    <Col style={{margin:'400px 50px 0px 0px'}}>
                            <ImageProduct title='300 ml -600 ml' description='Agua saborizada Limon'  src={aguaLimon} />
                        
                    </Col>
                    <Col>
                        <ImageProduct title='300 ml -600 ml' description='Agua saborizada Limonada de Coco' src={aguaLimonadaCoco} />
                    </Col>
                    <Col style={{margin:'400px 50px 0px 0px'}}>
                            <ImageProduct title='300 ml -600 ml' description='Agua saborizada Maracuya'  src={limonadaMaracuya} />
                        
                    </Col>
                </Row>
                <Row style={{background:'#F1EFF0',display:'flex',justifyContent:'space-between',height:'800px',marginTop:'10px'}}>
                    
                    <Col style={{margin:'400px 50px 0px 100px'}}>
                            <ImageProduct title='300 ml -600 ml' description='Refresco Passion-fruit'  src={refrescoFruit} />
                    </Col>
                    <Col>
                        <ImageProduct title='300 ml -600 ml' description='Refresco Salpicon' src={salpicon} />
                    </Col>
                    <Col style={{margin:'400px 50px 0px 0px'}}>
                            <ImageProduct title='300 ml -600 ml' description='Refresco Piña Naranja'  src={pinaNaranja} />
                    </Col>
                </Row>
                <Row style={{background:'#F1EFF0',display:'flex',justifyContent:'space-between',height:'800px',marginTop:'10px'}}>
                    
                    <Col style={{margin:'70px 0px 0px 30px'}}>
                        <Image 
                            src={poolCola} 
                            style={{width:'350px',height:'300px'}}    
                        />
                    </Col>
                    <Col style={{margin:'400px 0px 0px 0px'}}>
                        <Image 
                            src={poolNaranja} 
                            style={{width:'350px',height:'300px'}}    
                        />
                    </Col>
                    <Col style={{margin:'70px 0px 100px 0px'}}>
                        <Image 
                            src={poolLimonada} 
                            style={{width:'350px',height:'300px'}}    
                        />
                    </Col>
                </Row>
                <Row style={{background:'#F1EFF0',display:'flex',justifyContent:'space-between',height:'800px',marginTop:'10px'}}>
                    
                    <Col style={{margin:'70px 0px 0px 30px'}}>
                        <Image 
                            src={poolColombia} 
                            style={{width:'350px',height:'300px'}}    
                        />
                    </Col>
                    <Col style={{margin:'400px 0px 0px 0px'}}>
                        <Image 
                            src={poolUva} 
                            style={{width:'350px',height:'300px'}}    
                        />
                    </Col>
                    <Col style={{margin:'70px 0px 100px 0px'}}>
                        <Image 
                            src={poolManzana} 
                            style={{width:'350px',height:'300px'}}    
                        />
                    </Col>
                </Row>
        </Container>
    )
}