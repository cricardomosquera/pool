import { useEffect, useState } from 'react';
import {Row} from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import {Data,Vendedores} from '../../utils/data'
import { ItemsSellers } from '../../shared/CardsSellers/itemSellers';


export const Sellers=()=>{
    const text = Data.ventas
    const [sellers,setSeller]=useState<unknown | null>(<></>)
    useEffect(()=>{
        loadData()
    },[])
    const loadData=()=>{
        const row=[]
        for (let i=0 ; i<Vendedores.length;i++){
            //console.log("🚀 ~ loadData ~ clave:", Vendedores[i])
            row.push(
                <Row style={{width:'100%',background:'#DDE4EA'}}>

                    <Row style={{display:'flex',margin:'auto',justifyContent:'space-between',width:'80%'}}>
                        <ItemsSellers data={Vendedores[i]}/>
                        {i+1<Vendedores.length&&<ItemsSellers data={Vendedores[i+1]}/>}
                        
                    </Row>
                </Row>
            )
            //const key: string = clave; 
            //optiones[clave]
            //const value = Vendedores[key as keyof typeof Vendedores];
        }
        setSeller(row)    
    }
    return(
        <Container>
                <Row>
                    <div style={{display:'flex',height:'100%',width:'100%', backgroundColor:'#DDE4EA',marginTop:'10px'}}  >
                        <div style={{width:'100%',height:'10%',fontSize:30,marginTop:'30px'}}>
                            <Row>
                                {text.title}
                            </Row>
                            <Row style={{width:'50%',fontSize:20,margin:'auto'}}>
                                {text.description}
                            </Row>

                        </div>
                    </div>
                </Row>
                {sellers}
                <Row>

                </Row>
            </Container>
    )
}