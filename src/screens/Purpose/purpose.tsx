import {Image,Row, /* Col, */} from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import {Data} from '../../utils/data'
import { CardData } from '../../shared/CardData/cardData';
import valores from '../../assets/valores.jpeg'

export const Purpose=()=>{
    return(
        <Container>
            <Row style={{display:'flex'}}>
                <Image
                    style={{width:'80%',height:'500px'}} 
                    src={valores}/>

                <Row style={{background:'red'/* '#F1EFF0' */,display:'grid',width:'50%'}}>

                    <Row style={{/* width:'20%', */background:'#F1EFF0'/* ,margin:'auto' */}}>
                        <CardData data={Data.proposito}/>
                    </Row>
                </Row>
            </Row>

        </Container>
    )
}