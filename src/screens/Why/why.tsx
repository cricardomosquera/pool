import {Row,  Col,} from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import {Data} from '../../utils/data'

export const Why=()=>{
    const text = Data.porque
    const text1 = Data.porque1
    const text2 = Data.porque2
    return(
        <Container>
                <Row style={{display:'flex'}}>
                    <Col style={{width:'50%',display:'flex'}}>
                        <Col style={{
                            width:'500px',
                            height:'500px',
                            background:'#327EBD',
                            
                        }}>
                            <Row style={{
                                color:'white',
                                fontSize:20,
                                margin:'100px 0px 0px 0px'
                                }}>{text.title}</Row>
                            <Row style={{
                                color:'white',
                                fontSize:18,
                                margin:'50px 0px 0px 0px'}}>{text.description}</Row>
                        </Col>
                        <Col style={{
                                    width:'0px',
                                    height:'0px',
                                    borderTop:'250px solid transparent',
                                    borderLeft:'150px solid #327EBD',
                                    borderBottom:'250px solid transparent',
                        }}>                            </Col> 
                    </Col>
                    <Col style={{width:'50%'}}>
                        <Row style={{margin:'50px 0px 0px 100px',width:'70%'}}>
                            <div style={{fontFamily:'fantasy'}}>{text1.title}</div>
                            <div>{text1.description}</div>
                        </Row>
                        <Row style={{margin:'150px 0px 0px 100px',width:'70%'}}>
                            <div style={{fontFamily:'fantasy'}}>{text2.title}</div>
                            <div>{text2.description}</div>
                        </Row>
                    </Col>
                </Row>
        </Container>
    )
}