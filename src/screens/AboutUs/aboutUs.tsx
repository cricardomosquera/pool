import {Row,} from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import {Data} from '../../utils/data'
import { CardData } from '../../shared/CardData/cardData';
export const AboutUs=()=>{
    const text = Data.quienes

    return(
        <Container>
            <Row className="justify-content-md-center">
                <div style={{display:'flex',height:'100%',width:'100%', backgroundColor:'#F1EFF0'}}  >
                    <div style={{width:'20%',height:'10%'}}>
                       <div style={{
                                width:'0px',
                                height:'0px',
                                borderTop:'50px solid #327EBD',
                                borderLeft:'100px solid #327EBD',
                                borderRight:'30px solid transparent',
                                borderBottom:'50px solid #327EBD',
                       }}>
                        </div> 
                    </div>
                    <div style={{width:'80%',height:'10%',fontSize:30}}>
                        <Row>
                            {text.title}
                        </Row>
                        <Row style={{width:'80%',fontSize:20,marginLeft:'10%'}}>
                            {text.description}
                        </Row>

                    </div>
                </div>
            </Row>
                    <Row style={{background:'#F1EFF0'}}>
                        <Row style={{width:'70%',display:'flex',background:'#F1EFF0',justifyContent:'space-between',margin:'auto'}}>
                            <CardData data={Data.mision}/>
                            <CardData data={Data.vision}/>
                            {/* <Image src={mision}/> */}
                        </Row>
                    </Row>

        </Container>
    )
}