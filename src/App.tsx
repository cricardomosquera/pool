import './App.css'
import { Header } from './shared/Header/header'
import { AboutUs } from './screens/AboutUs/aboutUs';
import { Fisrt } from './screens/FirstHome/first'
import Container from 'react-bootstrap/Container';
import { Purpose } from './screens/Purpose/purpose';
import { Catalogue } from './screens/Catalogue/catalogue';
import { Sellers } from './screens/Sellers/seller';
import { Why } from './screens/Why/why';
import { Footer } from './shared/Footer/footer';
function App() {
  
  return (
      <Container>
          <Header/>
          <Fisrt/>
          <AboutUs/>
          <Purpose/>
          <Catalogue/>
          <Sellers/>
          <Why/>
          <Footer/>
      </Container>
      
  )
}
     

export default App
