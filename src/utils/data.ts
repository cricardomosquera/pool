import Angelo from '../assets/Vendedores/Angelo.jpg'
import David from '../assets/Vendedores/David.jpg'
import Gustavo from '../assets/Vendedores/Gustavo.jpg'
import Jaime from '../assets/Vendedores/Jaime.jpg'
import JuanFerney from '../assets/Vendedores/JuanFerney.jpg'
import LeidyCastro from '../assets/Vendedores/LeidyCastro.jpg'
import LeidyJohana from '../assets/Vendedores/LeidyJohana.jpg'
import Mario from '../assets/Vendedores/Mario.jpg'
import OscarCortez from '../assets/Vendedores/OscarCortez.jpg'
import OscarVanegas from '../assets/Vendedores/OscarVanegas.jpg'
import Viviana from '../assets/Vendedores/Viviana.jpg'

interface Vendedores{
    name:string
    cel:string
    image:string
    LuJue:string[]
    MarVie:string[]
    MieSab:string[]
}
interface Data{
    quienes:Items
    mision:Items
    vision:Items
    proposito:Items
    catagolo:Items
    ventas:Items
    porque:Items
    porque1:Items
    porque2:Items
}
interface Items{
    title:string
    description:string
}
export const Vendedores: Vendedores[]=[
    {
        name:'DAVID ESTEPHAN QUINTERO SANCHEZ',
        cel:'322 290 2614',
        image:David,
        LuJue:['JARDIN','VUGANVILES GUADAUALES COSTADO DERECHO DEL BATALLON- RIOJA','PASTRANA','OASIS ORIENTE',],
        MarVie:['GALINDO','CARBONEL','BRISAS DEL VENADO ALTO','MIRADOR VIRGILIO BARCO','TERCER MILENIO','LUIS CARLOS GALAN',],
        MieSab:['QUIRINAL','EL LAGO LSS DELICIAS','JOSE EUSTACION-CARACOLI','PLAZAS ALCID',]
    },
    {
        name:'VIVIANA VANESSA PALOMA',
        cel:'312 580 1031',
        image:Viviana,
        LuJue:['CALIXTO-GAITAN','LA-ISLA-1 DE MAYO','20 DE JULIO-7 DE AGOSTO','LAS COLINAS','LA INDEPENDENCIA','LAS BRISAS','LA LIBERTAD'],
        MarVie:['CANDIDO','SANTA INES','CAMILO TORRES-CALIFORNIA'],
        MieSab:['MANZANARE-TIMANCO','CANAIMA','SAN JORGE']
    },
    {
        name:'LEIDY CASTRO VEGA',
        cel:'304 211 8259',
        image:LeidyCastro,
        LuJue:['PALMAS 1 2 3',' CAMELIA',' MACHINES',],
        MarVie:['ALTICO','DIEGO DE OSPINA'],
        MieSab:['SUR MERCANEIVA','MOSCOVIA','SURABASTOS',]
    },
    {
        name:'LEIDY JOHANNA NARANJO',
        cel:'323 779 0034',
        image:LeidyJohana,
        LuJue:['CALAMARI','CHICALA','CORTIJO','UNA PARTE DE CANDIDO'],
        MarVie:['JOSE EUSTACIO-TENERIFE','CAMPO NUÑEZ-LA TOMA'],
        MieSab:['SANTA ISABEL-BOGOTA','GALAN-POZO AZUL LOMA LINDA','EMAYA TUQUILA']
    },
    {
        name:'JUAN FERNEY GARZON',
        cel:'318 370 6004',
        image:JuanFerney,
        LuJue:['PALERMO','COLOMBIA','ALPUJARRA','SAN ANDRES TELLO'],
        MarVie:['ALAMOS NORTE - PINO','VILLA CECILIA','QUINTAS DE SAN LUIS-PINAL','GRANJAS COMUNITARIAS-TRINIDAD'],
        MieSab:['SUR CUARTO CENTENARIO','OASIS SUR','LIMONAR']
    },
    {
        name:'OSCAR FERNANDO CORTES',
        cel:'315 502 7097',
        image:OscarCortez,
        LuJue:['TERUEL-VALENCIA','IQUIRA-FORTALECILLAS-HATO-NUEVO-POLONIA-VILLAVIEJA','DESIERTO-LA VICTORIA'],
        MarVie:['CAMPOALEGRE'],
        MieSab:['GUAJIRO','AIPE']
    },
    {
        name:'OSCAR ANDRES VANEGAS',
        cel:'304 230 1363',
        image:OscarVanegas,
        LuJue:['VILLA MAGDALENA','VILLA COLOMBIA','SANTA ROSA','DARIO CHANDIA ALREDEDORES'],
        MarVie:['CENTRO DESDE LA 7A HASTA LA CIRCUNVALAR','CALLE 1 HASTA LA TOMA'],
        MieSab:['SUR ORIENTALES','SAN MARTIN-ACACIAS-PEÑON REDONDIO','LOS PARQUES','ALFONSO LOPES-VENTILADOR']
    },
    {
        name:'ANGELO FORERO TELLEZ',
        cel:'301 605 8476',
        image:Angelo,
        LuJue:['SAN MIGUEL ARCANGEL-SIGLO 21- ALVARO URIBE-LA VICTORIA','PLAMAS 1',],
        MarVie:['ANDESITOS-LOS ANDES-GUALANDAY','LAS GRANJAS-CAMBULOS'],
        MieSab:['LAS ACACIAS-PEÑON REDONDO','PANORAMA-LA FLORIDA','DIVINO NIÑO-SIMON BOLIVAR-LAPAZ-CRISTALINAS-SAN CARLOS-LOS ALPES',]
    },
    {
        name:'MARIO ALVARO PERILLA',
        cel:'302 447 2342',
        image:Mario,
        LuJue:['RODRIGO LARA-VILLA DEL RIO-EL TRIANGULO-VEREDA CUISINDE','SANTA BARBARA Y ALREDEDORES-VILLA CONSTANZA HASTA PEAJE VIA A PALERMO HASTA LA BOMBA KM 3'],
        MarVie:['ZONA INDUSTRIAL-MIRA MAR PARTE DE CANAIMA HOSPITAL CLINICA DE LA POLICIA- SAN JORGE 2 Y FRONTERA DEL MILENIO.'],
        MieSab:['NUEVA GRANADA PARTE DE- LAS AMERICAS- BUENA VISTA - LA GAITANA- PRADO ALTO- LA FLORESTA-IPANEMA HASTA RESERVA DE LA CIERRA-VERGEL','VILLA REGINA-VICTOR FELIX',]
    },
    {
        name:'GUSTAVO GUTIERREZ',
        cel:'310 322 3257',
        image:Gustavo,
        LuJue:['ALGECIRAS'],
        MarVie:['CAMPOALEGRE','HOBO'],
        MieSab:['CASTILLA','COYAIME','JUNCAL','BETANIA','YAGUARA']
    },
    {
        name:'JAIME ENRIQUE BARREIRO',
        cel:'317 379 3243',
        image:Jaime,
        LuJue:['SANTA MARIA','PALERMO','BARAYA','TELLO'],
        MarVie:['RIO FRIO','RIVERA'],
        MieSab:['PUEBLO NUEVO','VELU-NATAGAIMA','PALMITAS-BRISAS-BALSILLAS',]
    },
    
]
export const Data:Data={
    quienes: {title: '¿Quienes somos?', description: 'En Rapiventas del Sur, nos enorgullece ser una destacada empresa distribuidora de gaseosas Pool, comprometida con la excelencia y la satisfacción del cliente. Desde el año 2023, hemos cultivado una reputación sólida y confiable en la industria, sirviendo a nuestros clientes con productos de alta calidad y un servicioexcepcional.' },
    mision: {title: 'Mision', description: '“La visión de Rapiventas del Sur es ser la empresa líder consolidándose como el proveedor preferido del mercado, generando bienestar a nuestros clientesofreciendovalor auténticoy redituable, manteniendo un nivel de calidad diferenciador y promoviendoel desarrollo de los empleados”.' },
    vision: {title: 'Vision', description: 'Rapiventas del Sur está dedicada a ubicarse, responsable y eficientemente, comola empresa líder del comercio de productos de consumo masivoa tiendas y supermercados en el Suroccidente del Huila y nororiente del Cauca; ofrecer al cliente calidad de servicioy valor genuinoy útil, con un sentidocomprometedor y humano por los clientes internos y la comunidad' },
    proposito:{title:'Proposito',description:'En Rapiventas del Sur, nos esforzamos por ser el motor de la excelencia en la distribución. Nuestro propósito es facilitar el acceso eficiente y confiable de productos excepcionales anuestros clientes en el departamento del Huila. Nos comprometemos a ser el socio de confianza que impulsa el éxito de nuestros clientes y contribuye al crecimiento sostenible de sus negocios.'},
    catagolo:{title:'Nuestro Catalogo',description:'Con nuestroaliado DRINKS DE COLOMBIA SAS, trabajamos unidos para ofrecer bebidas refrescantes y con un sabor único.'},
    ventas:{title:'Nuestroequipo de Venta',description:'Contamos conunexcelente equipo de vendedoresque dia a dia, trabajanarduamente para cumplir conlas expectativas denuestros clientes'},
    porque:{title:'¿Porque elegirnos?',description:'Nos destacamos porofrecer una combinación única de calidad, eficiencia y compromisocon nuestros clientes. Contamos buena experiencia en la industria y hemos construido una sólida reputación comolíderes en la distribución de productos de nuestras bebidas.'},
    porque1:{title:'1.Calidad de Productos:',description:'Trabajamos con los mejores fabricantes y proveedores para garantizar que nuestros productos cumplen con los más altos estándares de calidad. Nos esforzamospor ofrecer solucionesque superenlas expectativas denuestros clientes.'},
    porque2:{title:'2.Innovación Constante:',description:'Estamos comprometidos con la innovación continua. Buscamos constantemente nuevas formas de mejorar nuestros servicios y adoptamos tecnologías emergentes para mantenernos a la vanguardia de la industria.'}
}
