import {/* Button,Image, Col, */Row, } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';

interface Data{
    title:string
    description:string
}
interface Card{
    data:Data
}
export const CardData=({data}:Card) => {
    //console.log("🚀 ~ CardData ~ data:", data)
 

    return(
        <Container>
            
             <Row className="justify-content-md-center" 
                    style={{
                        width:'280px',
                        border:'solid',
                        borderWidth:4,
                        borderColor:'#327EBD', 
                        backgroundColor:'#F5F5E9',
                        borderRadius:'20%',
                        marginTop:'50px',
                        marginBottom:'50px'
                    }}>
                <Row style={{fontSize:30}}>
                    {data.title}
                </Row>
                <Row style={{fontSize:15,fontFamily:'fantasy'}}>
                    <Row style={{padding:'10px 20px 50px 20px '}}>
                        {data.description}
                    </Row>
                </Row>
             </Row>
        </Container>
    )
}