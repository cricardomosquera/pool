import {/* Button, Col, */Image,Row, } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';


interface ImageProduct{
    src:string
    height?:string    
    title:string
    description?:string
}
export const ImageProduct=({src,height='100px',title,description}:ImageProduct) => {
    console.log("🚀 ~ ImageProduct ~ data:", height)
 

    return(
        <Container>
            
             <Row className="justify-content-md-center">
                <Row style={{fontSize:30}}>
                    {title}
                </Row>
                <Row style={{fontSize:15,fontFamily:'fantasy'}}>
                    <Row style={{padding:'10px 20px 50px 20px '}}>
                    <Image 
                        src={src} 
                        style={{borderRadius:'50%',width:'200px',height:'200px'}}    
                    />
                    </Row>
                </Row>
                <Row style={{fontSize:30}}>
                    {description}
                </Row>
             </Row>
        </Container>
    )
}