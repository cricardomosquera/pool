import {Button,Image, Row, Col,} from 'react-bootstrap';
import pool from '../../assets/pool.png'
import Container from 'react-bootstrap/Container';
export const Header=()=>{


    return(
        <Container>
             <Row className="justify-content-md-center" style={{display:'flex',height:'20%', backgroundColor:'white'}}>
                <Col xs={6} sm={4} md={4} lg={4}>
                    <Image 
                        style={{ width: '30%' }} 
                        src={pool}     
                    />
                </Col>
                <div style={{display:'flex',width:'90%',justifyContent:'space-between',alignItems:'center'}}>
                 <Col  xs={12} sm={12} md={12} lg={2}>
                    <Button>
                    ¿Quienes Somos?
                    </Button>
                </Col>
                <Col md={4} xs={12} sm={4} lg={2}>
                    <Button>
                       Proposito 
                    </Button>
                </Col>
                <Col md={4} xs={12} sm={4} lg={2}>
                    <Button>
                        Catalogo
                    </Button>
                </Col>
                <Col md={4} xs={12} sm={4} lg={2}>
                    <Button>
                        Vendedores
                    </Button>
                </Col>
                <Col md={4} xs={12} sm={4} lg={2}>
                    <Button>
                        Contactanos
                    </Button>
                </Col>

                </div>
             </Row>
        </Container>
    )
}