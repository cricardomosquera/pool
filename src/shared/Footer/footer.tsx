import {Image, Row, Col,} from 'react-bootstrap';
import pool from '../../assets/pool.png'
import Container from 'react-bootstrap/Container';
import { SocialIcon } from 'react-social-icons'
export const Footer=()=>{


    return(
        <Container>
             <Row className="justify-content-md-center" 
                style={{
                    display:'flex',
                    height:'20%', 
                    backgroundColor:'#327EBD',
                    marginTop:'1%'
                    }}>
                
                <Col style={{display:'flex',width:'90%',justifyContent:'space-between',alignItems:'center'}}>
                    <Col style={{width:'50%'}} /* xs={12} sm={12} md={12} lg={2} */>
                        <Row style={{margin:'30px 0px 30px 30px'}}>
                            <a style={{display:'flex'}}>
                                <SocialIcon url="https://api.whatsapp.com/send/?phone=573147672141" />
                                <Row style={{color:'black',margin:'10px 0px 0px 5px'}}>
                                    3147672141
                                </Row>
                            </a>
                        </Row>
                        <Row style={{margin:'30px 0px 30px 30px'}}>
                            
                            <a style={{display:'flex'}}>
                            <SocialIcon url="https://www.facebook.com/profile.php?id=100068993663023 " />
                                <Row style={{color:'black',margin:'10px 0px 0px 5px'}}>
                                    Bebidas Pool
                                </Row>
                            </a>
                        </Row>
                        
                    </Col>
                    <Col  style={{width:'40%'}}/* xs={12} sm={12} md={12} lg={2} */>
                        <Row style={{margin:'30px 0px 30px 30px'}}>
                            <a style={{display:'flex'}}>
                                <SocialIcon url="https://www.tiktok.com/@pool.gaseosas?_t=8j9HeSsndIr&_r=1!" />
                                <Row style={{color:'black',margin:'10px 0px 0px 5px'}}>

                                    @pool.gaseosa
                                </Row>
                            </a>
                        </Row>
                        <Row style={{margin:'30px 0px 30px 30px'}}>
                            
                            <a style={{display:'flex'}}>
                            <SocialIcon url="https://www.instagram.com/pool_gaseosas?igsh=MXhhNWF5eWZ1dGo0cA==." />
                                <Row style={{color:'black',margin:'10px 0px 0px 5px'}}>
                                    pool_gaseosas
                                </Row>
                            </a>
                        </Row>
                        
                    </Col>
                    <Col /* xs={12} sm={12} md={12} lg={2} */>
                        <Row style={{margin:'30px 0px 30px 30px',color:'black'}}>
                            Cra3 B # 24 A - 124 Zona industrial - Detras de molinos Roa Neiva-Huila

                        </Row>
                        <Row style={{margin:'30px 0px 30px 30px'}}>
                            
                            <a style={{display:'flex'}}>
                            <SocialIcon url="https://www.google.com" />
                            <Row style={{color:'black',margin:'10px 0px 0px 5px'}}>
                                Rapiventasdelsur@gmail.com
                            </Row>
                            </a>
                        </Row>
                        
                    </Col>

                </Col>
                <Col xs={6} sm={4} md={4} lg={4}>
                    <Image 
                        style={{ width: '30%',margin:'30px 0px 30px 30px' }} 
                        src={pool}     
                    />
                </Col>
             </Row>
        </Container>
    )
}