import {Image, Col,Row, } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';

interface Data{
    name:string
    cel:string
    image:string
    LuJue:string[]
    MarVie:string[]
    MieSab:string[]
}
interface Card{
    data:Data
}
export const ItemsSellers=({data}:Card) => {
    console.log("🚀 ~ CardData ~ data:", data)
 

    return(
        <Container>
            
            <Row style={{margin:'10px 20px 50px 20px '}}>
                <Row style={{fontSize:15,fontFamily:'fantasy'}}>
                    <Row style={{padding:'10px 20px 50px 20px '}}>
                        <Image 
                            src={data.image} 
                            style={{borderRadius:'50%',width:'200px',height:'200px'}}    
                        />
                    </Row>
                </Row>
                <Row style={{fontSize:30,height:'80px'}}>
                    {data.name}
                </Row>
                <Row style={{fontSize:30}}>
                    {data.cel}
                </Row>
                <Row style={{
                    width:'400px',
                    height:'200px',
                    border:'solid',
                    borderWidth:4,
                    borderColor:'black',
                    display:'flex',
                    margin:'auto' 
                }}>
                    <Col style={{width:'33%'}}>
                        <Row style={{
                            fontSize:15,
                            border:'solid',
                            borderRightWidth:4,
                            borderBottomWidth:4,
                            borderColor:'black',
                            height:'10%',
                            }}>
                            Lunes - Jueves
                        </Row>
                        <Row style={{
                            fontSize:13,
                            border:'solid',
                            borderRightWidth:4,
                            borderColor:'black',
                            height:'86%',
                            }}>
                            {data.LuJue.map((data,index)=>
                                    <Row key={index}>{data}</Row>
                                )} 
                        </Row>
                    </Col>
                    <Col style={{width:'33%'}}>
                        <Row style={{
                            fontSize:15,
                            border:'solid',
                            borderRightWidth:4,
                            borderBottomWidth:4,
                            borderColor:'black',
                            height:'10%'
                            }}>
                            Martes -Viernes
                        </Row>
                        <Row 
                            style={{
                                fontSize:13,
                                height:'86%',
                                border:'solid',
                                borderRightWidth:4,
                                borderColor:'black',
                            }}
                        >
                            {data.MarVie.map((data,index)=>
                                    <Row key={index}>{data}</Row>
                                )} 
                        </Row>
                    </Col>
                    <Col style={{width:'34%'}}>
                        <Row 
                            style={{
                                fontSize:15,
                                border:'solid',
                                borderBottomWidth:5,
                                borderColor:'black',
                                height:'10%',
                                }}>
                            Miercoles -Sabado
                        </Row>
                        <Row 
                            style={{
                                fontSize:15,
                                height:'86%',
                                }}>
                            {data.MieSab.map((data,index)=>
                                    <Row key={index}>{data}</Row>
                                )} 
                        </Row>
                    </Col>
                </Row>
             </Row>
        </Container>
    )
}